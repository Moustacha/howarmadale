import calendar
from datetime import datetime, date

from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db.models import Sum
from django.views.generic import TemplateView
from rest_framework import serializers
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from armadale.models import Suburb, CrimeStatistic


class CompareView(TemplateView):

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['years'] = range(1999,datetime.now().year+1)
        context_data['months'] = list(zip(range(1,13), calendar.month_name[1:]))
        
        return context_data
    
    
class SuburbSerialiser(serializers.ModelSerializer):
    class Meta:
        model = Suburb
        fields = ('name',)
    
    
    
class SuburbListAPIView(ListAPIView):
    serializer_class = SuburbSerialiser
    
    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Suburb.objects.all()
        search_term = self.request.query_params.get('term', None)
        if search_term is not None:
            queryset = queryset.filter(name__istartswith=search_term)
        return queryset


class CompareAPIView(APIView):
    def get(self, request, *args, **kwargs):
        """
        Returns totals for the periods given and suburbs chosen
        """
        suburbs = request.GET.getlist('suburb')        
        dateFrom = date(int(request.GET.get('yearFrom', None)), int(request.GET.get('monthFrom', None)), 1)
        dateTo = date(int(request.GET.get('yearTo', None)), int(request.GET.get('monthTo', None)), 1)
        
        results = []
        messages = []
        
        for suburb in suburbs:
            try :
                sub = Suburb.objects.get(name__iexact=suburb)
                
                qres = CrimeStatistic.objects.filter(suburb__name__iexact=suburb,
                                              date__range=(dateFrom, dateTo)).aggregate(Sum('assault'),
                                                                          Sum('burg_dwelling'),
                                                                          Sum('burg_other'),
                                                                          Sum('graffiti'),
                                                                          Sum('robbery'),
                                                                          Sum('smv'))
                qres['suburb'] = sub.name
                results.append(qres)
            except (MultipleObjectsReturned, ObjectDoesNotExist) as e:
                messages.append('Could not find suburb "'+suburb+'"')
        
        response = Response({'messages': messages, 'results': results}, status=status.HTTP_200_OK)
        return response