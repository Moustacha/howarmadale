# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.db import migrations, models, transaction


class Migration(migrations.Migration):

    dependencies = [
        ('armadale', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='crimestatistic',
            name='date',
            field=models.DateField(default=datetime.date(2016, 2, 22)),
            preserve_default=False,
        ),
        migrations.RunSQL("UPDATE armadale_crimestatistic SET date=to_date(year||'-'||month||'-1', 'YYYY-MM-DD'); commit;"),
        migrations.AlterUniqueTogether(
            name='crimestatistic',
            unique_together=set([('date', 'suburb')]),
        ),
        
        migrations.RemoveField(
            model_name='crimestatistic',
            name='month',
        ),
        migrations.RemoveField(
            model_name='crimestatistic',
            name='year',
        ),
    ]
