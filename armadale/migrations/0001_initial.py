# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CrimeStatistic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('year', models.IntegerField()),
                ('month', models.IntegerField()),
                ('assault', models.IntegerField()),
                ('burg_dwelling', models.IntegerField(verbose_name='Burglary (Dwelling)')),
                ('burg_other', models.IntegerField(verbose_name='Burglary (Other)')),
                ('graffiti', models.IntegerField()),
                ('robbery', models.IntegerField()),
                ('smv', models.IntegerField(verbose_name='Steal Motor Vehicle')),
            ],
        ),
        migrations.CreateModel(
            name='Suburb',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=250, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='crimestatistic',
            name='suburb',
            field=models.ForeignKey(to='armadale.Suburb'),
        ),
        migrations.AlterUniqueTogether(
            name='crimestatistic',
            unique_together=set([('year', 'month', 'suburb')]),
        ),
    ]
