# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

def cleanup_suburbs(apps, schema_editor):
    Suburb = apps.get_model("armadale", "Suburb")
    Suburb.objects.filter(name__in=('BaLGA', 'WiLGA')).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('armadale', '0002_auto_20160222_1028'),
    ]

    operations = [
        migrations.RunPython(cleanup_suburbs)
    ]
