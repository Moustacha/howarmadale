from django.db import models

# Create your models here.

class Suburb(models.Model):
    name = models.CharField(max_length=250, unique=True)
    
    def __str__(self):
        return self.name


class CrimeStatistic(models.Model):
    date = models.DateField()
    suburb = models.ForeignKey(Suburb)
    
    assault = models.IntegerField()
    burg_dwelling = models.IntegerField(verbose_name='Burglary (Dwelling)')
    burg_other = models.IntegerField(verbose_name='Burglary (Other)')
    graffiti = models.IntegerField()
    robbery = models.IntegerField()
    smv = models.IntegerField(verbose_name='Steal Motor Vehicle')
    
    class Meta:
        unique_together = (('date', 'suburb'),)
        
    def __str__(self):
        return '{}: {}'.format(self.suburb.name, self.date)