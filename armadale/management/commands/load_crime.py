from django.core.management import BaseCommand
import requests
from armadale.models import Suburb, CrimeStatistic
from lxml import etree
from datetime import datetime, date
from time import strptime
from django.db import transaction
from multiprocessing.dummy import Pool as ThreadPool
import itertools

SUBURB_JSON_URL = 'https://www.police.wa.gov.au/apiws/Json/GetChildItemData/A19F4922-E640-418A-87E7-5AE50077D536'
CRIME_URL = 'https://www.police.wa.gov.au/Crime/Statistics?locality={suburb:s}&startYear=1999&startMonth=1&endYear={year:d}&endMonth=12'


def get_crime_statistics(suburb, current_year):
    response = requests.get(CRIME_URL.format(suburb=suburb, year=current_year))
    tree   = etree.HTML(response.text)
    crime_table = tree.xpath('//*[@id="page-content"]/article/div[2]/div/table/tbody')[0]
    
    results = []
    for row in crime_table:
        year = row[0].text
        # Get the month as a number
        month = strptime(row[1].text, '%b').tm_mon
        assault = row[2].text
        burg_dwel = row[3].text
        burg_other = row[4].text
        graffiti = row[5].text
        robbery = row[6].text
        smv = row[7].text
        
        defaults = {'assault': assault,
                    'burg_dwelling': burg_dwel,
                    'burg_other': burg_other,
                    'graffiti': graffiti,
                    'robbery': robbery,
                    'smv': smv}
        
        results.append({'year': year,
                        'month': month,
                        'defaults': defaults})
        
    print('Retrieved crime stats for '+suburb)
    return (suburb, results)

class Command(BaseCommand):
    help = 'Update the database with fresh crime statistics'
    
    def handle(self, *args, **options):
        # Get the suburbs from WAPOL
        response = requests.get(SUBURB_JSON_URL)
        suburb_data = response.json()
        suburb_names = {x['Name'].title() for x in suburb_data['Data']} # A set of normalised names
        
        
        # Update the suburbs in the database
        with transaction.atomic():
            for suburb in suburb_names:
                if not Suburb.objects.filter(name=suburb).exists():
                    Suburb.objects.create(name=suburb)
                    self.stdout.write('Created Suburb "{}"'.format(suburb))
        
        current_year = datetime.now().year
        # Process each suburb to get its crime data
        pool = ThreadPool(16)
        results = pool.starmap(get_crime_statistics, zip(suburb_names, itertools.repeat(current_year)))
        with transaction.atomic():
            for res in results:
                suburb = res[0]
                for cs in res[1]:
                    date = date(cs['year'], cs['month'], 1)
                    CrimeStatistic.objects.update_or_create(date=date, suburb=Suburb.objects.get(name=suburb), defaults=cs['defaults'])
                self.stdout.write('Updated CrimeStatistics for "{}"'.format(suburb))
