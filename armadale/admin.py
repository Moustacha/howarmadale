from django.contrib import admin

from armadale.models import Suburb, CrimeStatistic


# Register your models here.
admin.site.register([Suburb, CrimeStatistic])