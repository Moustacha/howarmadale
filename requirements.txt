Django>=1.8,<1.9
psycopg2==2.6.1
whitenoise==2.0.6
django-bootstrap3==6.2.2
requests==2.9.1
djangorestframework==3.3.2
lxml==3.2.5