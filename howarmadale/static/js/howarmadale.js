$(document).ready(function() {
	
	$compareForm = $('#compareForm');

	$compareForm.on('submit', function(evt) {
		evt.preventDefault();
		
		
		// Get the data...		
		$.getJSON('compare', $compareForm.serialize(), function(data) {
			if(data.messages.length == 0) {
				var datasets = [];
				$.each(data.results, function(i, val) {
					datasets.push({
						name: val.suburb,
						data: [val.assault__sum, val.burg_dwelling__sum, val.burg_other__sum,
						       val.graffiti__sum, val.robbery__sum, val.smv__sum]
					});
				});
				
				var suburbOne = data.results[0].suburb;
				var suburbTwo = data.results[1].suburb;
				
				// Show it in the chart
				var $chart = $('#chart');
				$chart.highcharts({
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: suburbOne+' compared to '+suburbTwo
			        },
			        xAxis: {
			            categories: ['Assault', 'Burglary (Dwelling)', 'Burglary (Other)',
							         'Graffiti', 'Robbery', 'Steal Motor Vehicle']
			        },
			        yAxis: {
			            title: {
			                text: 'Incidents'
			            }
			        },
			        series: datasets
			    });
				window.scrollTo(0, $chart.offset().top);
			} else {
				var errorMessages = '';
				$.each(data.messages, function(i, val) {
					errorMessages += '<h3>'+val+'</h3>';
				});
				$('#chart').html(errorMessages);
			}
		});
		
		
	});
});


$(function() {
  var cache = {};
  $( "#suburbOne, #suburbTwo" ).autocomplete({
    minLength: 3,
    source: function( request, response ) {
      var term = request.term;
      if ( term in cache ) {
        response( cache[ term ] );
        return;
      }
      $.getJSON( "suburbs", request, function( data, status, xhr ) {
      	var new_data = [];
      	$.each(data, function(i, val) { 
			new_data.push(val.name);
		});
        cache[ term ] = new_data;
        response( new_data );
      });
    }
  });
});