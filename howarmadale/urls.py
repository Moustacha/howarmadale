from django.conf import settings
from django.conf.urls import include, url, patterns
from django.contrib import admin

from armadale.views import CompareView, SuburbListAPIView, CompareAPIView


urlpatterns = [
    # Examples:
    # url(r'^$', 'howarmadale.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # Home page
    url(r'^$', CompareView.as_view(template_name='home.html')),
    
    # REST
    url(r'^suburbs/$', SuburbListAPIView.as_view()),
    url(r'^compare/$', CompareAPIView.as_view()),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )